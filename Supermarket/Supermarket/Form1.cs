﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supermarketi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var pos = this.PointToScreen(label1.Location);
            pos = pictureBox1.PointToClient(pos);
            label1.Parent = pictureBox1;
            label1.Location = pos;
            label1.BackColor = Color.Transparent;

            var pos1 = this.PointToScreen(label2.Location);
            pos1 = pictureBox1.PointToClient(pos1);
            label2.Parent = pictureBox1;
            label2.Location = pos1;
            label2.BackColor = Color.Transparent;

        }
        private void pozadinaHIDE()
        {
            pictureBox1.Visible = false;
            label1.Visible = false;
            label2.Visible = false;
        }
        private void pozadinaSHOW()
        {
            pictureBox1.Visible = true;
            label1.Visible = true;
            label2.Visible = true;
        }
        private void mniPodaciSupermarketa_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmSupermarket supermarketForm = new frmSupermarket();
            supermarketForm.MdiParent = this;
            supermarketForm.WindowState = FormWindowState.Maximized;
            supermarketForm.ControlBox = false;
            supermarketForm.Show();
            pozadinaHIDE();
        }

        private void mniPodaciMenadzeri_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmMenadzer menadzerForm = new frmMenadzer();
            menadzerForm.MdiParent = this;
            menadzerForm.WindowState = FormWindowState.Maximized;
            menadzerForm.ControlBox = false;
            menadzerForm.Show();
            pozadinaHIDE();
        }
        private void mniPodaciArtikli_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmArtikl artiklForm = new frmArtikl();
            artiklForm.MdiParent = this;
            artiklForm.WindowState = FormWindowState.Maximized;
            artiklForm.ControlBox = false;
            artiklForm.Show();
            pozadinaHIDE();
        }

        private void mniIzvestajiSupermarketa_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmIzvestaj izvestajForm = new frmIzvestaj();
            izvestajForm.MdiParent = this;
            izvestajForm.WindowState = FormWindowState.Maximized;
            izvestajForm.ControlBox = false;
            izvestajForm.Show();
            pozadinaHIDE();
        }


        private void CloseAllForms()
        {
            Form[] formToClose = null;
            int i = 1;
            foreach (Form form in Application.OpenForms)
            {
                if (form != this)
                {
                    Array.Resize(ref formToClose, i);
                    formToClose[i - 1] = form;
                    i++;
                }
            }
            if (formToClose != null)
                for (int j = 0; j < formToClose.Length; j++)
                    formToClose[j].Dispose();
        }

        private void mniPodaciDistributeri_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmDistributer distributerForm = new frmDistributer();
            distributerForm.MdiParent = this;
            distributerForm.WindowState = FormWindowState.Maximized;
            distributerForm.ControlBox = false;
            distributerForm.Show();
            pozadinaHIDE();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmDistributer distributerForm = new frmDistributer();
            distributerForm.MdiParent = this;
            distributerForm.WindowState = FormWindowState.Maximized;
            distributerForm.ControlBox = false;
            distributerForm.Show();
            pozadinaHIDE();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmMenadzer menadzerForm = new frmMenadzer();
            menadzerForm.MdiParent = this;
            menadzerForm.WindowState = FormWindowState.Maximized;
            menadzerForm.ControlBox = false;
            menadzerForm.Show();
            pozadinaHIDE();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmSupermarket supermarketForm = new frmSupermarket();
            supermarketForm.MdiParent = this;
            supermarketForm.WindowState = FormWindowState.Maximized;
            supermarketForm.ControlBox = false;
            supermarketForm.Show();
            pozadinaHIDE();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmArtikl artiklForm = new frmArtikl();
            artiklForm.MdiParent = this;
            artiklForm.WindowState = FormWindowState.Maximized;
            artiklForm.ControlBox = false;
            artiklForm.Show();
            pozadinaHIDE();
        }

        private void vrsteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmVrsta vrstaForm = new frmVrsta();
            vrstaForm.MdiParent = this;
            vrstaForm.WindowState = FormWindowState.Maximized;
            vrstaForm.ControlBox = false;
            vrstaForm.Show();
            pozadinaHIDE();

        }

        private void uveziToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmDistributer distributerForm = new frmDistributer();
            distributerForm.MdiParent = this;
            distributerForm.WindowState = FormWindowState.Maximized;
            distributerForm.ControlBox = false;
            distributerForm.Show();
            pozadinaHIDE();
        }

        private void izveziToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllForms();
            frmDistributer distributerForm = new frmDistributer();
            distributerForm.MdiParent = this;
            distributerForm.WindowState = FormWindowState.Maximized;
            distributerForm.ControlBox = false;
            distributerForm.Show();
            pozadinaHIDE();
        }

        private void hOMEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllForms();

            pozadinaSHOW();
        }

        private void supermarketiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmIzvestaj izvestajFrm = new frmIzvestaj();
            izvestajFrm.Show();

        }
    }
}
