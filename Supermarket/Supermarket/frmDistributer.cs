﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supermarketi
{
    public partial class frmDistributer : Form
    {
        List<Distributer> distributeriList = new List<Distributer>();
        string akcija = "";
        int indeksSelektovanog = -1;
        public frmDistributer()
        {
            InitializeComponent();
            dgDistributeri.AllowUserToAddRows = false;
            dgDistributeri.AllowUserToDeleteRows = false;
            dgDistributeri.ReadOnly = true;
            dgDistributeri.AutoGenerateColumns = false;

            dgDistributeri.Columns.Add("ID", "ID");
            dgDistributeri.Columns["ID"].Visible = false;
            dgDistributeri.Columns.Add("imeDistributera", "ImeD");
            dgDistributeri.Columns.Add("drzavaDistributera", "Drzava");
            dgDistributeri.Columns.Add("mestoDistributera", "Mesto");
            dgDistributeri.Columns.Add("adresaDistributera", "Adresa");
            dgDistributeri.Columns.Add("pttDistributera", "Ptt");
            dgDistributeri.Columns.Add("telefonDistributera", "Telefon");


            txtDisabled();
            btnChangeEnabled();
            btnSubmitDisabled();

            prikaziDistributereDGV();
        }
        private void txtDisabled()
        {
            txtIme.Enabled = false;
            txtDrzava.Enabled = false;
            txtMesto.Enabled = false;
            txtAdresa.Enabled = false;
            txtPtt.Enabled = false;
            txtTelefon.Enabled = false;
        }

        private void txtEnabled()
        {
            txtIme.Enabled = true;
            txtDrzava.Enabled = true;
            txtMesto.Enabled = true;
            txtAdresa.Enabled = true;
            txtPtt.Enabled = true;
            txtTelefon.Enabled = true;
        }

        private void btnChangeDisabled()
        {
            btnDodaj.Enabled = false;
            btnPromeni.Enabled = false;
            btnObrisi.Enabled = false;
        }

        private void btnChangeEnabled()
        {
            btnDodaj.Enabled = true;
            btnPromeni.Enabled = true;
            btnObrisi.Enabled = true;
        }

        private void btnSubmitDisabled()
        {
            btnPotvrdi.Enabled = false;
            btnOdustani.Enabled = false;
        }

        private void btnSubmitEnabled()
        {
            btnPotvrdi.Enabled = true;
            btnOdustani.Enabled = true;
        }

        private void ponistiUnosTxt()
        {
            txtIme.Text = "";
            txtDrzava.Text = "";
            txtMesto.Text = "";
            txtAdresa.Text = "";
            txtPtt.Text = "";
            txtTelefon.Text = "";
        }
        private void prikaziDistributeraTxt()
        {
            int idSelektovanog = (int)dgDistributeri.SelectedRows[0].Cells["ID"].Value;
            Distributer selektovaniDistributer =
                distributeriList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
            if (selektovaniDistributer != null)
            {
                txtIme.Text = selektovaniDistributer.ImeD;
                txtDrzava.Text = selektovaniDistributer.Drzava;
                txtMesto.Text = selektovaniDistributer.Mesto;
                txtAdresa.Text = selektovaniDistributer.Adresa;
                txtPtt.Text = selektovaniDistributer.Ptt.ToString();
                txtTelefon.Text = selektovaniDistributer.Telefon;
            }
        }
        private void prikaziDistributereDGV()
        {
            distributeriList = new Distributer().ucitajDistributere();
            dgDistributeri.Rows.Clear();
            for (int i = 0; i < distributeriList.Count; i++)
            {
                dgDistributeri.Rows.Add();
                dgDistributeri.Rows[i].Cells["ID"].Value =
                    distributeriList[i].ID;
                dgDistributeri.Rows[i].Cells["imeDistributera"].Value =
                    distributeriList[i].ImeD;
                dgDistributeri.Rows[i].Cells["drzavaDistributera"].Value =
                    distributeriList[i].Drzava;
                dgDistributeri.Rows[i].Cells["mestoDistributera"].Value =
                    distributeriList[i].Mesto;
                dgDistributeri.Rows[i].Cells["adresaDistributera"].Value =
                    distributeriList[i].Adresa;
                dgDistributeri.Rows[i].Cells["pttDistributera"].Value =
                    distributeriList[i].Ptt;
                dgDistributeri.Rows[i].Cells["telefonDistributera"].Value =
                    distributeriList[i].Telefon;

            }
            ponistiUnosTxt();
            dgDistributeri.CurrentCell = null;
            if (distributeriList.Count > 0)
            {
                if (indeksSelektovanog != -1)
                    dgDistributeri.Rows[indeksSelektovanog].Selected = true;
                else
                    dgDistributeri.Rows[0].Selected = true;
                prikaziDistributeraTxt();
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            ponistiUnosTxt();
            txtEnabled();
            btnSubmitEnabled();
            btnChangeDisabled();
            akcija = "dodaj";
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgDistributeri.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Da li zelite da obrisete odabranog distributera?",
                "Potvrda brisanja", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int idSelektovanog = (int)dgDistributeri.SelectedRows[0].Cells["ID"].Value;
                    Distributer selektovaniDstributer = distributeriList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
                    if (selektovaniDstributer != null)
                    {
                        selektovaniDstributer.obrisiDistributera();
                    }
                    indeksSelektovanog = -1;
                    prikaziDistributereDGV();
                }
            }
            else
            {
                MessageBox.Show("Nema unetih podataka");
            }
        }

        private void btnPromeni_Click(object sender, EventArgs e)
        {
            if (dgDistributeri.SelectedRows.Count > 0)
            {
                txtEnabled();
                btnSubmitEnabled();
                btnChangeDisabled();
                akcija = "promeni";
            }
            else
            {
                MessageBox.Show("Nema unetih podataka");
            }
        }

        private void btnPotvrdi_Click(object sender, EventArgs e)
        {
            try
            {
                if (akcija == "promeni")
                {
                    int idSelektovanog = (int)dgDistributeri.SelectedRows[0].Cells["ID"].Value;
                    Distributer selektovaniDistributer = distributeriList.Where(x => x.ID == idSelektovanog).FirstOrDefault();

                    selektovaniDistributer.ImeD = txtIme.Text;
                    selektovaniDistributer.Drzava = txtDrzava.Text;
                    selektovaniDistributer.Mesto = txtMesto.Text;
                    selektovaniDistributer.Adresa = txtAdresa.Text;
                    selektovaniDistributer.Ptt = Int32.Parse(txtPtt.Text);
                    selektovaniDistributer.Telefon = txtTelefon.Text;
                    selektovaniDistributer.azurirajDistributera();
                    indeksSelektovanog = dgDistributeri.SelectedRows[0].Index;
                }
                else if (akcija == "dodaj")
                {
                    Distributer dist = new Distributer();
                    dist.ImeD = txtIme.Text;
                    dist.Drzava = txtDrzava.Text;
                    dist.Mesto = txtMesto.Text;
                    dist.Adresa = txtAdresa.Text;
                    dist.Ptt = Int32.Parse(txtPtt.Text);
                    dist.Telefon = txtTelefon.Text;
                    dist.dodajDistributera();
                    indeksSelektovanog = dgDistributeri.Rows.Count;
                }
                txtDisabled();
                btnSubmitDisabled();
                btnChangeEnabled();
                akcija = "";
                prikaziDistributereDGV();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            txtDisabled();
            btnSubmitDisabled();
            btnChangeEnabled();
        }

        private void dgDistributeri_CellClick(object sender,
            DataGridViewCellEventArgs e)
        {
            if (dgDistributeri.CurrentRow != null)
            {
                dgDistributeri.Rows[dgDistributeri.CurrentRow.Index].Selected = true;
                prikaziDistributeraTxt();
            }

        }

        private void btnUvezi_Click(object sender, EventArgs e)
        {

            OpenFileDialog oDlg = new OpenFileDialog();
            oDlg.InitialDirectory = "C:\\";
            oDlg.Filter = "xml Files (*.xml)|*.xml";
            if (DialogResult.OK == oDlg.ShowDialog())
            {
                DistributeriXML.uveziXML(oDlg.FileName);
            }
            prikaziDistributereDGV();
        }

        private void btnIzvezi_Click(object sender, EventArgs e)
        {
            SaveFileDialog sDlg = new SaveFileDialog();
            sDlg.InitialDirectory = "C:\\";
            sDlg.Filter = "xml Files (*.xml)|*.xml";
            if (DialogResult.OK == sDlg.ShowDialog())
            {
                DistributeriXML.izveziXML(sDlg.FileName, distributeriList);
            }
        }
    }
}
