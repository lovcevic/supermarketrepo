﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Supermarketi
{
    class DistributeriXML
    {


        public static void uveziXML(string putanja)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(putanja);
            XmlNodeList distributeriNodes =
                xmlDoc.GetElementsByTagName("Distributer");
            foreach (XmlNode distributerNode in distributeriNodes)
            {
                Distributer dist = new Distributer();

                dist.ImeD = distributerNode.ChildNodes[0].InnerText;
                dist.Drzava = distributerNode.ChildNodes[1].InnerText;
                dist.Mesto = distributerNode.ChildNodes[2].InnerText;
                dist.Adresa = distributerNode.ChildNodes[3].InnerText;
                dist.Ptt = Convert.ToInt32(distributerNode.ChildNodes[4].InnerText);
                dist.Telefon = distributerNode.ChildNodes[5].InnerText;
                dist.dodajDistributera();
            }
        }
        public static Boolean izveziXML(string putanja, List<Distributer> distributeri)
        {
            XmlDocument xmlDoc = new XmlDocument();

            XmlTextWriter xmlWriter =
                new XmlTextWriter(putanja, System.Text.Encoding.UTF8);

            xmlWriter.WriteProcessingInstruction(
                "xml", "version='1.0' encoding='UTF-8'");

            xmlWriter.WriteStartElement("Distributeri");

            xmlWriter.Close();
            xmlDoc.Load(putanja);


            foreach (Distributer dist in distributeri)
            {
                XmlElement distributerNode = xmlDoc.CreateElement("Distributer");

                XmlElement imeDistributera = xmlDoc.CreateElement("ImeDistributera");
                imeDistributera.InnerText = dist.ImeD;
                distributerNode.AppendChild(imeDistributera);

                XmlElement drzavaDistributera = xmlDoc.CreateElement("DrzavaDistributera");
                drzavaDistributera.InnerText = dist.Drzava;
                distributerNode.AppendChild(drzavaDistributera);

                XmlElement mestoDistributera =
                    xmlDoc.CreateElement("MestoDistributera");
                mestoDistributera.InnerText = dist.Mesto;
                distributerNode.AppendChild(mestoDistributera);

                XmlElement adresaDistributera = xmlDoc.CreateElement("AdresaDistributera");
                adresaDistributera.InnerText = dist.Adresa;
                distributerNode.AppendChild(adresaDistributera);

                XmlElement pttDistributera = xmlDoc.CreateElement("PttDistributera");
                pttDistributera.InnerText = dist.Ptt.ToString();
                distributerNode.AppendChild(pttDistributera);

                XmlElement telefonDistributera = xmlDoc.CreateElement("TelefonDistributera");
                telefonDistributera.InnerText = dist.Telefon;
                distributerNode.AppendChild(telefonDistributera);

                xmlDoc.DocumentElement.InsertAfter(
                    distributerNode, xmlDoc.DocumentElement.LastChild);
            }
            xmlDoc.Save(putanja);
            return true;
        }

    }
}



