﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supermarketi
{
    public partial class frmSupermarket : Form
    {
        List<Supermarket> supermarketiList = new List<Supermarket>();
        string akcija = "";
        int indeksSelektovanog = -1;

        public frmSupermarket()
        {
            InitializeComponent();
            dgSupermarket.AllowUserToAddRows = false;
            dgSupermarket.AllowUserToDeleteRows = false;
            dgSupermarket.ReadOnly = true;
            dgSupermarket.AutoGenerateColumns = false;

            dgSupermarket.Columns.Add("ID", "ID");
            dgSupermarket.Columns["ID"].Visible = false;
            dgSupermarket.Columns.Add("imeSupermarketa", "Ime");
            dgSupermarket.Columns.Add("mestoSupermarketa", "Mesto");
            dgSupermarket.Columns.Add("adresaSupermarketa", "Adresa");
            dgSupermarket.Columns.Add("telefonSupermarketa", "Telefon");
            dgSupermarket.Columns.Add("imeMenadzera", "Ime menadzera");
            dgSupermarket.Columns.Add("prezimeMenadzera", "Prezime menadzera");
            dgSupermarket.Columns.Add("telefonMenadzera", "Telefon menadzera");

            List<Menadzer> menadzeriList = new Menadzer().ucitajMenadzere();
            cbxMenadzer.Items.Add(new DictionaryEntry("Odaberite menadzera", 0));
            foreach (Menadzer men in menadzeriList)
            {
                cbxMenadzer.Items.Add(new DictionaryEntry(
                    men.ImeM + " " + men.Prezime, men.ID));
            }
            cbxMenadzer.DisplayMember = "Key";
            cbxMenadzer.ValueMember = "Value";
            cbxMenadzer.DataSource = cbxMenadzer.Items;

            txtDisabled();
            btnChangeEnabled();
            btnSubmitDisabled();

            prikaziSupermarketeDGV();
        }

        private void txtDisabled()
        {
            txtIme.Enabled = false;
            txtMesto.Enabled = false;
            txtAdresa.Enabled = false;
            txtTelefon.Enabled = false;
            cbxMenadzer.Enabled = false;

        }

        private void txtEnabled()
        {
            txtIme.Enabled = true;
            txtMesto.Enabled = true;
            txtAdresa.Enabled = true;
            txtTelefon.Enabled = true;
            cbxMenadzer.Enabled = true;
        }

        private void btnChangeDisabled()
        {
            btnDodaj.Enabled = false;
            btnPromeni.Enabled = false;
            btnObrisi.Enabled = false;
        }

        private void btnChangeEnabled()
        {
            btnDodaj.Enabled = true;
            btnPromeni.Enabled = true;
            btnObrisi.Enabled = true;
        }

        private void btnSubmitDisabled()
        {
            btnPotvrdi.Enabled = false;
            btnOdustani.Enabled = false;
        }

        private void btnSubmitEnabled()
        {
            btnPotvrdi.Enabled = true;
            btnOdustani.Enabled = true;
        }

        private void ponistiUnosTxt()
        {
            txtIme.Text = "";
            txtMesto.Text = "";
            txtAdresa.Text = "";
            txtTelefon.Text = "";
            cbxMenadzer.SelectedIndex = 0;
        }

        private void prikaziSupermarketTxt()
        {
            int idSelektovanog = (int)dgSupermarket.SelectedRows[0].Cells["ID"].Value;
            Supermarket selektovaniSupermarket =
                supermarketiList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
            if (selektovaniSupermarket != null)
            {
                txtIme.Text = selektovaniSupermarket.Ime;
                txtMesto.Text = selektovaniSupermarket.Mesto;
                txtAdresa.Text = selektovaniSupermarket.Adresa;
                txtTelefon.Text = selektovaniSupermarket.Telefon;
                int menIndex = cbxMenadzer.FindString(
                    selektovaniSupermarket.Menadzer.ImeM + " " +
                    selektovaniSupermarket.Menadzer.Prezime);
                cbxMenadzer.SelectedIndex = menIndex;
            }
        }

        private void prikaziSupermarketeDGV()
        {
            supermarketiList = new Supermarket().ucitajSupermarkete();
            dgSupermarket.Rows.Clear();
            for (int i = 0; i < supermarketiList.Count; i++)
            {
                dgSupermarket.Rows.Add();
                dgSupermarket.Rows[i].Cells["ID"].Value =
                    supermarketiList[i].ID;
                dgSupermarket.Rows[i].Cells["imeSupermarketa"].Value =
                    supermarketiList[i].Ime;
                dgSupermarket.Rows[i].Cells["mestoSupermarketa"].Value =
                    supermarketiList[i].Mesto;
                dgSupermarket.Rows[i].Cells["adresaSupermarketa"].Value =
                    supermarketiList[i].Adresa;
                dgSupermarket.Rows[i].Cells["telefonSupermarketa"].Value =
                    supermarketiList[i].Telefon;
                dgSupermarket.Rows[i].Cells["imeMenadzera"].Value =
                    supermarketiList[i].Menadzer.ImeM;
                dgSupermarket.Rows[i].Cells["prezimeMenadzera"].Value =
                    supermarketiList[i].Menadzer.Prezime;
                dgSupermarket.Rows[i].Cells["telefonMenadzera"].Value =
                  supermarketiList[i].Menadzer.TelefonM;
            }
            ponistiUnosTxt();
            dgSupermarket.CurrentCell = null;
            if (supermarketiList.Count > 0)
            {
                if (indeksSelektovanog != -1)
                    dgSupermarket.Rows[indeksSelektovanog].Selected = true;
                else
                    dgSupermarket.Rows[0].Selected = true;
                prikaziSupermarketTxt();
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            ponistiUnosTxt();
            txtEnabled();
            btnSubmitEnabled();
            btnChangeDisabled();
            akcija = "dodaj";
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgSupermarket.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Da li zelite da obrisete odabranu supermarket?",
                "Potvrda brisanja", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int idSelektovanog = (int)dgSupermarket.SelectedRows[0].Cells["ID"].Value;
                    Supermarket selektovanisupermarket = supermarketiList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
                    if (selektovanisupermarket != null)
                    {
                        selektovanisupermarket.obrisiSupermarket();
                    }
                    indeksSelektovanog = -1;
                    prikaziSupermarketeDGV();
                }
            }
            else
            {
                MessageBox.Show("Nema unetih podataka");
            }
        }

        private void btnPromeni_Click(object sender, EventArgs e)
        {
            if (dgSupermarket.SelectedRows.Count > 0)
            {
                txtEnabled();
                btnSubmitEnabled();
                btnChangeDisabled();
                akcija = "promeni";
            }
            else
            {
                MessageBox.Show("Nema unetih podataka");
            }
        }

        private void btnPotvrdi_Click(object sender, EventArgs e)
        {
            try
            {
                if (akcija == "promeni")
                {
                    int idSelektovanog = (int)dgSupermarket.SelectedRows[0].Cells["ID"].Value;
                    Supermarket selektovaniSupermarket = supermarketiList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
                    selektovaniSupermarket.Ime = txtIme.Text;
                    selektovaniSupermarket.Mesto = txtMesto.Text;
                    selektovaniSupermarket.Adresa = txtAdresa.Text;
                    selektovaniSupermarket.Telefon = txtTelefon.Text;

                    Menadzer men = new Menadzer();
                    men.ID = Int32.Parse(cbxMenadzer.SelectedValue.ToString());
                    selektovaniSupermarket.Menadzer = men;
                    selektovaniSupermarket.azurirajSupermarket();
                    indeksSelektovanog = dgSupermarket.SelectedRows[0].Index;
                }
                else if (akcija == "dodaj")
                {
                    Supermarket super = new Supermarket();
                    super.Ime = txtIme.Text;
                    super.Mesto = txtMesto.Text;
                    super.Adresa = txtAdresa.Text;
                    super.Telefon = txtTelefon.Text;

                    Menadzer men = new Menadzer();
                    men.ID = Int32.Parse(cbxMenadzer.SelectedValue.ToString());
                    super.Menadzer = men;
                    super.dodajSupermarket();
                    indeksSelektovanog = dgSupermarket.Rows.Count;
                }
                txtDisabled();
                btnSubmitDisabled();
                btnChangeEnabled();
                prikaziSupermarketeDGV();
            }
            catch (FormatException)
            {
                MessageBox.Show("Neispravan format");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            txtDisabled();
            btnSubmitDisabled();
            btnChangeEnabled();
        }

        private void dgSupermarket_CellClick(object sender,
            DataGridViewCellEventArgs e)
        {
            if (dgSupermarket.CurrentRow != null)
            {
                dgSupermarket.Rows[dgSupermarket.CurrentRow.Index].Selected = true;
                prikaziSupermarketTxt();
            }
        }
        private void CloseAllForms()
        {
            Form[] formToClose = null;
            int i = 1;
            foreach (Form form in Application.OpenForms)
            {
                if (form != this)
                {
                    Array.Resize(ref formToClose, i);
                    formToClose[i - 1] = form;
                    i++;
                }
            }
            if (formToClose != null)
                for (int j = 0; j < formToClose.Length; j++)
                    formToClose[j].Dispose();
        }

    }
}

