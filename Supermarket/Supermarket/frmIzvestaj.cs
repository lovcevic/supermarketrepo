﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supermarketi
{
    public partial class frmIzvestaj : Form
    {
        

        public frmIzvestaj()
        {
            InitializeComponent();

            List<Menadzer> menadzeriList = new Menadzer().ucitajMenadzere();

            cbxMenadzeri.Items.Add(new DictionaryEntry("Svi menadzeri", 0));

            foreach (Menadzer men in menadzeriList)
            {

                cbxMenadzeri.Items.Add(new DictionaryEntry(men.ImeM + " " + men.Prezime, men.ID));
            }

            cbxMenadzeri.DisplayMember = "Key";
            cbxMenadzeri.ValueMember = "Value";

            cbxMenadzeri.DataSource = cbxMenadzeri.Items;

        }

        private void frmIzvestaj_Load(object sender, EventArgs e)
        {
            SupermarketBindingSource.DataSource = new Supermarket().ucitajSupermarkete();
            this.reportViewer1.RefreshReport();
        }

        private void cbxMenadzeri_SelectedIndexChanged(object sender, EventArgs e)
        {

            int itemValue = Int32.Parse(cbxMenadzeri.SelectedValue.ToString());

            if (itemValue == 0)
                SupermarketBindingSource.DataSource = new Supermarket().ucitajSupermarkete();
            else
                SupermarketBindingSource.DataSource =
                    new Supermarket().ucitajSupermarketeZaMenadzera(itemValue);
            this.reportViewer1.RefreshReport();
        }

     
    }
}
