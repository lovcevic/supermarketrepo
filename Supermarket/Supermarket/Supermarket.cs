﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supermarketi
{
    [Serializable]
    class Supermarket
    {
        private int id;
        private string ime;
        private string mesto;
        private string adresa;
        private string telefon;
        private Menadzer menadzer;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Ime
        {
            get { return ime; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti ime supermarketa!!!");
                ime = value;
            }
        }

        public string Mesto
        {
            get { return mesto; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti mesto supermarketa!!!");
                mesto = value;
            }
        }

        public string Adresa
        {
            get { return adresa; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti adresu supermarketa!!!");
                adresa = value;
            }
        }

        public string Telefon
        {
            get { return telefon; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti broj telefona!!!");
                telefon = value;
            }
        }
        public Menadzer Menadzer
        {
            get { return menadzer; }
            set
            {
                if (value.ID == 0)
                    throw new Exception("Morate uneti menadzera!!!");
                menadzer = value;
            }
        }

        private string _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\SupermarketiDB.mdf;Integrated Security=True";

        public void dodajSupermarket()
        {
            string insertSql = "INSERT INTO T_SUPERMARKET " +
                "(Ime, Mesto, Adresa, Telefon, MenadzerId) VALUES " +
                "(@Ime, @Mesto, @Adresa, @Telefon, @MenadzerId)";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = insertSql;
                command.Parameters.Add(new SqlParameter("@Ime", Ime));
                command.Parameters.Add(new SqlParameter("@Mesto", Mesto));
                command.Parameters.Add(new SqlParameter("@Adresa", Adresa));
                command.Parameters.Add(new SqlParameter("@Telefon", Telefon));
                command.Parameters.Add(new SqlParameter("@MenadzerId", Menadzer.ID));
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void azurirajSupermarket()
        {
            string updateSql =
                "UPDATE T_SUPERMARKET " +
                "SET Ime = @Ime, Mesto= @Mesto, Adresa = @Adresa, Telefon = @Telefon, MenadzerId = @MenadzerId " +
                "WHERE SupermarketId = @SupermarketId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = updateSql;
                command.Parameters.Add(new SqlParameter("@SupermarketId", ID));
                command.Parameters.Add(new SqlParameter("@Ime", Ime));
                command.Parameters.Add(new SqlParameter("@Mesto", Mesto));
                command.Parameters.Add(new SqlParameter("@Adresa", Adresa));
                command.Parameters.Add(new SqlParameter("@Telefon", Telefon));
                command.Parameters.Add(new SqlParameter("@MenadzerId", Menadzer.ID));
                connection.Open();
                command.ExecuteNonQuery();
            }

        }

        public void obrisiSupermarket()
        {
            StringBuilder errorMessages = new StringBuilder();
            string deleteSql =
                "DELETE FROM T_SUPERMARKET WHERE SupermarketId = @SupermarketId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = deleteSql;
                command.Parameters.Add(new SqlParameter("@SupermarketId", ID));
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (SqlException)
                {


                    MessageBox.Show("podatak je vezan za supermarket");
                }
            }
        }

        public List<Supermarket> ucitajSupermarkete()
        {
            List<Supermarket> supermarketi = new List<Supermarket>();
            string queryString =
                "SELECT " +
                " super.SupermarketId, super.Ime, super.Mesto, super.Adresa, super.Telefon, " +
                " men.MenadzerId, men.ImeM, men.Prezime, men.TelefonM " +
                "FROM T_SUPERMARKET super, T_MENADZER men WHERE super.MenadzerId = men.MenadzerId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    Supermarket supermarket;
                    while (reader.Read())
                    {
                        supermarket = new Supermarket();
                        supermarket.ID = Int32.Parse(reader["SupermarketId"].ToString());
                        supermarket.Ime = reader["Ime"].ToString();
                        supermarket.Mesto = reader["Mesto"].ToString();
                        supermarket.Adresa = reader["Adresa"].ToString();
                        supermarket.Telefon = reader["Telefon"].ToString();
                        supermarket.menadzer = new Menadzer();
                        supermarket.menadzer.ID = Int32.Parse(reader["MenadzerId"].ToString());
                        supermarket.menadzer.ImeM = reader["ImeM"].ToString();
                        supermarket.menadzer.Prezime = reader["Prezime"].ToString();
                        supermarket.menadzer.TelefonM = reader["TelefonM"].ToString();

                        supermarketi.Add(supermarket);
                    }
                }
            }
            return supermarketi;
        }

        public List<Supermarket> ucitajSupermarketeZaMenadzera(int menadzerId)
        {
            List<Supermarket> supermarketi = new List<Supermarket>();
            string queryString =
                "SELECT " +
                " super.SupermarketId, super.Ime, super.Mesto, super.Adresa, super.Telefon, " +
                " men.MenadzerId, men.ImeM, men.Prezime, men.TelefonM " +
                "FROM T_SUPERMARKET super, T_MENADZER men WHERE super.MenadzerId = men.MenadzerId AND men.MenadzerId = @MenadzerId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                command.Parameters.Add(new SqlParameter("@MenadzerId", menadzerId));
                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {

                    Supermarket super;
                    while (reader.Read())
                    {
                        super = new Supermarket();
                        super.ID = Int32.Parse(reader["SupermarketId"].ToString());
                        super.Ime = reader["Ime"].ToString();
                        super.Mesto = reader["Mesto"].ToString();
                        super.Adresa = reader["Adresa"].ToString();
                        super.Telefon = reader["Telefon"].ToString();

                        super.menadzer = new Menadzer();
                        super.menadzer.ID = Int32.Parse(reader["MenadzerId"].ToString());
                        super.menadzer.ImeM = reader["ImeM"].ToString();
                        super.menadzer.Prezime = reader["Prezime"].ToString();
                        super.menadzer.TelefonM = reader["TelefonM"].ToString();

                        supermarketi.Add(super);

                    }
                }
            }
            return supermarketi;
        }
    }
}
