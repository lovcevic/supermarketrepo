﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarketi
{
    [Serializable]
    class Vrsta
    {

        private int id;
        private string nazivV;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string NazivV
        {
            get { return nazivV; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti vrstu artikla!!!");
                nazivV = value;
            }
        }
        private string _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\SupermarketiDB.mdf;Integrated Security=True";

        public void dodajVrstu()
        {
            string insertSql = "INSERT INTO T_VRSTA " +
                "(NazivV) VALUES (@NazivV)";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = insertSql;
                command.Parameters.Add(new SqlParameter("@NazivV", NazivV));
                connection.Open();
                command.ExecuteNonQuery();
            }

        }

        public void azurirajVrstu()
        {
            string updateSql =
                "UPDATE T_VRSTA " +
                "SET NazivV = @NazivV " +
                "WHERE VrstaId = @VrstaId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = updateSql;
                command.Parameters.Add(new SqlParameter("@VrstaId", ID));
                command.Parameters.Add(new SqlParameter("@NazivV", NazivV));

                connection.Open();
                command.ExecuteNonQuery();
            }

        }

        public void obrisiVrstu()
        {
            string deleteSql =
                "DELETE FROM T_VRSTA WHERE VrstaId = @VrstaId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = deleteSql;
                command.Parameters.Add(new SqlParameter("@VrstaId", ID));
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public List<Vrsta> ucitajVrste()
        {
            List<Vrsta> vrste = new List<Vrsta>();
            string queryString =
                "SELECT * FROM T_VRSTA;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    Vrsta vrsta;
                    while (reader.Read())
                    {
                        vrsta = new Vrsta();
                        vrsta.ID = Int32.Parse(reader["VrstaId"].ToString());
                        vrsta.NazivV = reader["NazivV"].ToString();
                        vrste.Add(vrsta);
                    }
                }
            }
            return vrste;
        }


    }
}
