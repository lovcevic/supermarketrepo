﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supermarketi
{
    [Serializable]
    class Menadzer
    {
        private int id;
        private string imeM;
        private string prezime;
        private string telefonM;


        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string ImeM
        {
            get { return imeM; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti ime menadzera!!!");
                imeM = value;
            }
        }

        public string Prezime
        {
            get { return prezime; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti prezime menadzera!!!");
                prezime = value;
            }
        }

        public string TelefonM
        {
            get { return telefonM; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti telefon menadzera!!!");
                telefonM = value;
            }
        }





        private string _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\SupermarketiDB.mdf;Integrated Security=True";

        public void dodajMenadzera()
        {
            string insertSql = "INSERT INTO T_MENADZER " +
                "(ImeM, Prezime, TelefonM) VALUES " +
                "(@ImeM, @Prezime, @TelefonM)";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = insertSql;
                command.Parameters.Add(new SqlParameter("@ImeM", ImeM));
                command.Parameters.Add(new SqlParameter("@Prezime", Prezime));
                command.Parameters.Add(new SqlParameter("@TelefonM", TelefonM));
                connection.Open();
                command.ExecuteNonQuery();
            }

        }

        public void azurirajMenadzera()
        {

            string updateSql =
                "UPDATE T_MENADZER " +
                "SET ImeM= @ImeM, Prezime = @Prezime, TelefonM = @TelefonM " +
                "WHERE MenadzerId = @MenadzerId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = updateSql;
                command.Parameters.Add(new SqlParameter("@MenadzerId", ID));
                command.Parameters.Add(new SqlParameter("@ImeM", ImeM));
                command.Parameters.Add(new SqlParameter("@Prezime", Prezime));
                command.Parameters.Add(new SqlParameter("@TelefonM", TelefonM));

                connection.Open();
                command.ExecuteNonQuery();



            }
        }

        public void obrisiMenadzera()
        {

            StringBuilder errorMessages = new StringBuilder();

            string deleteSql =
                "DELETE FROM T_MENADZER WHERE MenadzerId = @MenadzerId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {

                SqlCommand command = connection.CreateCommand();
                command.CommandText = deleteSql;
                command.Parameters.Add(new SqlParameter("@MenadzerId", ID));
                try
                {

                    connection.Open();
                    command.ExecuteNonQuery();
                }

                catch (SqlException)
                {


                    MessageBox.Show("podatak je vezan za supermarket");
                }


            }
        }

        public List<Menadzer> ucitajMenadzere()
        {
            List<Menadzer> menadzeri = new List<Menadzer>();
            string queryString =
                "SELECT * FROM T_MENADZER;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    Menadzer men;
                    while (reader.Read())
                    {
                        men = new Menadzer();
                        men.ID = Int32.Parse(reader["MenadzerId"].ToString());
                        men.ImeM = reader["ImeM"].ToString();
                        men.Prezime = reader["Prezime"].ToString();
                        men.TelefonM = reader["TelefonM"].ToString();
                        menadzeri.Add(men);
                    }
                }
            }
            return menadzeri;
        }
    }
}
