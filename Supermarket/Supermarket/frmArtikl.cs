﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supermarketi
{
    public partial class frmArtikl : Form
    {
        List<Artikl> artikliList = new List<Artikl>();
        string akcija = "";
        int indeksSelektovanog = -1;
        public frmArtikl()
        {
            InitializeComponent();
            dgArtikli.AllowUserToAddRows = false;
            dgArtikli.AllowUserToDeleteRows = false;
            dgArtikli.ReadOnly = true;
            dgArtikli.AutoGenerateColumns = false;

            dgArtikli.AllowUserToAddRows = false;
            dgArtikli.AllowUserToDeleteRows = false;
            dgArtikli.ReadOnly = true;
            dgArtikli.AutoGenerateColumns = false;

            dgSupermarketi.AllowUserToAddRows = false;
            dgSupermarketi.AllowUserToDeleteRows = false;
            dgSupermarketi.ReadOnly = true;
            dgSupermarketi.AutoGenerateColumns = false;

            dgSupermarketi.AllowUserToAddRows = false;
            dgSupermarketi.AllowUserToDeleteRows = false;
            dgSupermarketi.ReadOnly = true;
            dgSupermarketi.AutoGenerateColumns = false;

            dgArtikli.Columns.Add("ID", "ID");
            dgArtikli.Columns["ID"].Visible = false;
            dgArtikli.Columns.Add("naziv", "Naziv");
            dgArtikli.Columns.Add("cena", "Cena");
            dgArtikli.Columns.Add("imeDistributera", "Distributer");
            dgArtikli.Columns.Add("drzavaDistributera", "Drzava Distributera");
            dgArtikli.Columns.Add("vrstaArtikla", "Vrsta");




            dgSupermarketi.Columns.Add("imeSupermarketa", "Uneti Supermarketi");
            dgSupermarketi.Columns.Add("mestoSupermarketa", "Mesto");
            dgSupermarketi.Columns.Add("adresaSupermarketa", "Adresa");


            List<Distributer> distributeriList = new Distributer().ucitajDistributere();
            cbxDistributer.Items.Add(new DictionaryEntry("Odaberite distributera", 0));
            foreach (Distributer dist in distributeriList)
            {
                cbxDistributer.Items.Add(new DictionaryEntry(
                    dist.ImeD + " " + dist.Drzava, dist.ID));
            }
            cbxDistributer.DisplayMember = "Key";
            cbxDistributer.ValueMember = "Value";
            cbxDistributer.DataSource = cbxDistributer.Items;


            List<Vrsta> vrsteList = new Vrsta().ucitajVrste();
            cbxVrsta.Items.Add(new DictionaryEntry("Odaberite vrstu", 0));
            foreach (Vrsta vrsta in vrsteList)
            {
                cbxVrsta.Items.Add(new DictionaryEntry(
                    vrsta.NazivV, vrsta.ID));
            }
            cbxVrsta.DisplayMember = "Key";
            cbxVrsta.ValueMember = "Value";
            cbxVrsta.DataSource = cbxVrsta.Items;


            txtDisabled();
            btnChangeEnabled();
            btnSubmitDisabled();


            List<Supermarket> supermarketiList = new Supermarket().ucitajSupermarkete();
            foreach (Supermarket supermarket in supermarketiList)
            {
                clbxSupermarketi.Items.Add(new DictionaryEntry(
                    supermarket.Ime + "(" + supermarket.Mesto + ")", supermarket.ID));
            }
            clbxSupermarketi.DisplayMember = "Key";
            clbxSupermarketi.ValueMember = "Value";
            clbxSupermarketi.DataSource = clbxSupermarketi.Items;

            txtDisabled();
            btnChangeEnabled();
            btnSubmitDisabled();

            prikaziArtiklDGV();
            prikaziSupermarketDGV();
        }
        private void txtDisabled()
        {

            txtNaziv.Enabled = false;
            txtCena.Enabled = false;
            clbxSupermarketi.Enabled = false;
            cbxDistributer.Enabled = false;
            cbxVrsta.Enabled = false;

        }

        private void txtEnabled()
        {
            txtNaziv.Enabled = true;
            txtCena.Enabled = true;
            clbxSupermarketi.Enabled = true;
            cbxDistributer.Enabled = true;
            cbxVrsta.Enabled = true;

        }

        private void btnChangeDisabled()
        {
            btnDodaj.Enabled = false;
            btnPromeni.Enabled = false;
            btnObrisi.Enabled = false;
        }

        private void btnChangeEnabled()
        {
            btnDodaj.Enabled = true;
            btnPromeni.Enabled = true;
            btnObrisi.Enabled = true;
        }

        private void btnSubmitDisabled()
        {
            btnPotvrdi.Enabled = false;
            btnOdustani.Enabled = false;
        }

        private void btnSubmitEnabled()
        {
            btnPotvrdi.Enabled = true;
            btnOdustani.Enabled = true;
        }

        private void ponistiUnosTxt()
        {

            txtNaziv.Text = "";
            txtCena.Text = "";
            foreach (int i in clbxSupermarketi.CheckedIndices)
            {
                clbxSupermarketi.SetItemCheckState(i, CheckState.Unchecked);
            }
        }

        private void prikaziArtiklTxt()
        {
            int idSelektovanog = (int)dgArtikli.SelectedRows[0].Cells["ID"].Value;
            Artikl selektovaniArtikl =
                artikliList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
            if (selektovaniArtikl != null)
            {
                txtNaziv.Text = selektovaniArtikl.Naziv;
                txtCena.Text = selektovaniArtikl.Cena;


                int distIndex = cbxDistributer.FindString(
                    selektovaniArtikl.Distributer.ImeD + " " +
                    selektovaniArtikl.Distributer.Drzava);
                cbxDistributer.SelectedIndex = distIndex;

                int vrstaIndex = cbxVrsta.FindString(
                selektovaniArtikl.Vrsta.NazivV);
                cbxVrsta.SelectedIndex = vrstaIndex;




                foreach (int i in clbxSupermarketi.CheckedIndices)
                {
                    clbxSupermarketi.SetItemCheckState(i, CheckState.Unchecked);
                }
                foreach (Supermarket super in selektovaniArtikl.Supermarketi)
                {
                    int listIndeks = clbxSupermarketi.FindString(super.Ime);
                    clbxSupermarketi.SetItemCheckState(listIndeks, CheckState.Checked);
                }
            }
        }

        void prikaziArtiklDGV()
        {
            artikliList = new Artikl().ucitajArtikle();
            dgArtikli.Rows.Clear();
            for (int i = 0; i < artikliList.Count; i++)
            {
                dgArtikli.Rows.Add();
                dgArtikli.Rows[i].Cells["ID"].Value =
                    artikliList[i].ID;
                dgArtikli.Rows[i].Cells["naziv"].Value =
                    artikliList[i].Naziv;
                dgArtikli.Rows[i].Cells["cena"].Value =
                    artikliList[i].Cena;
                dgArtikli.Rows[i].Cells["imeDistributera"].Value =
                    artikliList[i].Distributer.ImeD;
                dgArtikli.Rows[i].Cells["drzavaDistributera"].Value =
                    artikliList[i].Distributer.Drzava;
                dgArtikli.Rows[i].Cells["vrstaArtikla"].Value =
                   artikliList[i].Vrsta.NazivV;

            }
            ponistiUnosTxt();
            dgArtikli.CurrentCell = null;
            if (artikliList.Count > 0)
            {
                if (indeksSelektovanog != -1)
                    dgArtikli.Rows[indeksSelektovanog].Selected = true;
                else
                    dgArtikli.Rows[0].Selected = true;
                prikaziArtiklTxt();
            }
        }

        private void prikaziSupermarketDGV()
        {
            if (dgArtikli.SelectedRows.Count > 0)
            {
                int idSelektovanog = (int)dgArtikli.SelectedRows[0].Cells["ID"].Value;
                Artikl selektovaniArtikl =
                    artikliList.Where(x => x.ID == idSelektovanog).FirstOrDefault();

                List<Supermarket> supermarketi = selektovaniArtikl.Supermarketi;

                dgSupermarketi.Rows.Clear();
                for (int i = 0; i < supermarketi.Count; i++)
                {
                    dgSupermarketi.Rows.Add();
                    dgSupermarketi.Rows[i].Cells["imeSupermarketa"].Value =
                        supermarketi[i].Ime;
                    dgSupermarketi.Rows[i].Cells["mestoSupermarketa"].Value =
                        supermarketi[i].Mesto;
                    dgSupermarketi.Rows[i].Cells["adresaSupermarketa"].Value =
                        supermarketi[i].Adresa;
                }
            }
            else
            {
                dgSupermarketi.Rows.Clear();
            }
        }
        private void btnDodaj_Click(object sender, EventArgs e)
        {
            ponistiUnosTxt();
            txtEnabled();
            btnSubmitEnabled();
            btnChangeDisabled();
            akcija = "dodaj";
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgArtikli.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Da li zelite da obrisete odabrani artikl?",
                "Potvrda brisanja", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int idSelektovanog = (int)dgArtikli.SelectedRows[0].Cells["ID"].Value;
                    Artikl selektovaniArtikl =
                        artikliList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
                    selektovaniArtikl.obrisiArtikl();
                    indeksSelektovanog = -1;
                    prikaziArtiklDGV();
                    prikaziSupermarketDGV();
                }
            }
            else
            {
                MessageBox.Show("Nema unetih podataka");
            }
        }

        private void btnPromeni_Click(object sender, EventArgs e)
        {
            if (dgArtikli.SelectedRows.Count > 0)
            {
                txtEnabled();
                btnSubmitEnabled();
                btnChangeDisabled();
                akcija = "promeni";
            }
            else
            {
                MessageBox.Show("Nema unetih podataka");
            }
        }

        private void btnPotvrdi_Click(object sender, EventArgs e)
        {
            try
            {
                if (akcija == "promeni")
                {
                    int idSelektovanog = (int)dgArtikli.SelectedRows[0].Cells["ID"].Value;
                    Artikl selektovaniArtikl =
                        artikliList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
                    selektovaniArtikl.Naziv = txtNaziv.Text;
                    selektovaniArtikl.Cena = txtCena.Text;

                    selektovaniArtikl.Distributer = new Distributer();
                    selektovaniArtikl.Distributer.ID = Int32.Parse(cbxDistributer.SelectedValue.ToString());

                    selektovaniArtikl.Vrsta = new Vrsta();
                    selektovaniArtikl.Vrsta.ID = Int32.Parse(cbxVrsta.SelectedValue.ToString());



                    selektovaniArtikl.Supermarketi = new List<Supermarket>();
                    foreach (var item in clbxSupermarketi.CheckedItems)
                    {
                        int superId = (int)((DictionaryEntry)item).Value;
                        selektovaniArtikl.Supermarketi.Add(new Supermarket() { ID = superId });
                    }
                    selektovaniArtikl.azurirajArtikl();
                    indeksSelektovanog = dgArtikli.SelectedRows[0].Index;
                }
                else if (akcija == "dodaj")
                {
                    Artikl artikl = new Artikl();

                    artikl.Naziv = txtNaziv.Text;
                    artikl.Cena = txtCena.Text;

                    artikl.Distributer = new Distributer();
                    artikl.Distributer.ID = Int32.Parse(cbxDistributer.SelectedValue.ToString());

                    artikl.Vrsta = new Vrsta();
                    artikl.Vrsta.ID = Int32.Parse(cbxVrsta.SelectedValue.ToString());
                    artikl.Supermarketi = new List<Supermarket>();
                    foreach (var item in clbxSupermarketi.CheckedItems)
                    {
                        int superId = (int)((DictionaryEntry)item).Value;
                        artikl.Supermarketi.Add(new Supermarket() { ID = superId });
                    }
                    artikl.dodajArtikl();
                    indeksSelektovanog = dgArtikli.Rows.Count;
                }
                txtDisabled();
                btnSubmitDisabled();
                btnChangeEnabled();
                prikaziArtiklDGV();
                prikaziSupermarketDGV();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            txtDisabled();
            btnSubmitDisabled();
            btnChangeEnabled();
        }

        private void dgArtikli_CellClick(object sender,
            DataGridViewCellEventArgs e)
        {
            if (dgArtikli.CurrentRow != null)
            {
                dgArtikli.Rows[dgArtikli.CurrentRow.Index].Selected = true;
                prikaziArtiklTxt();
                prikaziSupermarketDGV();
            }
        }



    }
}

