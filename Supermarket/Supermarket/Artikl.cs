﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarketi
{
    [Serializable]
    class Artikl
    {
        private int id;
        private string naziv;
        private string cena;
        private List<Supermarket> supermarketi;
        private Distributer distributer;
        private Vrsta vrsta;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Naziv
        {
            get { return naziv; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti naziv artikla!!!");
                naziv = value;
            }
        }

        public string Cena
        {
            get { return cena; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti Cenu!!!");
                cena = value;
            }
        }



        public List<Supermarket> Supermarketi
        {
            get { return supermarketi; }
            set { supermarketi = value; }
        }

        public Distributer Distributer
        {
            get { return distributer; }
            set
            {
                if (value == null)
                    throw new Exception("Morate uneti ime distributera!!!");
                distributer = value;
            }
        }

        public Vrsta Vrsta
        {
            get { return vrsta; }
            set
            {
                if (value == null)
                    throw new Exception("Morate uneti vrstu artikla!!!");
                vrsta = value;
            }

        }



        private string _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\SupermarketiDB.mdf;Integrated Security=True";

        public void dodajArtikl()
        {
            string insertArtiklSql = "INSERT INTO T_ARTIKL " +
                "(DistributerId, VrstaId, Naziv, Cena) VALUES " +
                "(@DistributerId, @VrstaId, @Naziv, @Cena);" +
                "SELECT CAST(SCOPE_IDENTITY() AS INT);";


            string insertArtiklSupermarketSql = "INSERT INTO T_SUPERMARKET_ARTIKL " +
                "(SupermarketId, ArtiklId) VALUES " +
                "(@SupermarketId, @ArtiklId);";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand artiklCommand = connection.CreateCommand();
                artiklCommand.CommandText = insertArtiklSql;
                artiklCommand.Parameters.Add(new SqlParameter("@DistributerId", Distributer.ID));
                artiklCommand.Parameters.Add(new SqlParameter("@VrstaId", Vrsta.ID));
                artiklCommand.Parameters.Add(new SqlParameter("@Naziv", Naziv));
                artiklCommand.Parameters.Add(new SqlParameter("@Cena", Cena));


                connection.Open();
                int insertedId = (int)artiklCommand.ExecuteScalar();

                SqlCommand artiklSupermarketCommand = connection.CreateCommand();
                artiklSupermarketCommand.CommandText = insertArtiklSupermarketSql;
                foreach (Supermarket super in Supermarketi)
                {
                    artiklSupermarketCommand.Parameters.Clear();
                    artiklSupermarketCommand.Parameters.Add(new SqlParameter("@SupermarketId", super.ID));
                    artiklSupermarketCommand.Parameters.Add(new SqlParameter("@ArtiklId", insertedId));
                    artiklSupermarketCommand.ExecuteNonQuery();
                }
            }
        }

        public void azurirajArtikl()
        {
            string updateArtiklSql =
              "UPDATE T_ARTIKL " +
              "SET DistributerId = @DistributerId, VrstaId = @VrstaId, Naziv = @Naziv, Cena= @Cena " +
              "WHERE ArtiklId = @ArtiklId;";

            string deleteArtiklSupermarketSql =
                "DELETE T_SUPERMARKET_ARTIKL WHERE ArtiklId = @ArtiklId";
            string insertArtiklSupermarketSql =
                "INSERT INTO T_SUPERMARKET_ARTIKL " +
                "(SupermarketId, ArtiklId) VALUES " +
                "(@SupermarketId, @ArtiklId);";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand updateCommand = connection.CreateCommand();
                updateCommand.CommandText = updateArtiklSql;
                updateCommand.Parameters.Add(new SqlParameter("@ArtiklId", ID));
                updateCommand.Parameters.Add(new SqlParameter("@DistributerId", Distributer.ID));
                updateCommand.Parameters.Add(new SqlParameter("@VrstaId", Vrsta.ID));
                updateCommand.Parameters.Add(new SqlParameter("@Naziv", Naziv));
                updateCommand.Parameters.Add(new SqlParameter("@Cena", Cena));


                connection.Open();
                updateCommand.ExecuteNonQuery();

                SqlCommand deleteCommand = connection.CreateCommand();
                deleteCommand.CommandText = deleteArtiklSupermarketSql;
                deleteCommand.Parameters.Add(new SqlParameter("@ArtiklId", ID));
                deleteCommand.ExecuteNonQuery();

                SqlCommand artiklSupermarketCommand = connection.CreateCommand();
                artiklSupermarketCommand.CommandText = insertArtiklSupermarketSql;
                foreach (Supermarket super in Supermarketi)
                {
                    artiklSupermarketCommand.Parameters.Clear();
                    artiklSupermarketCommand.Parameters.Add(new SqlParameter("@SupermarketId", super.ID));
                    artiklSupermarketCommand.Parameters.Add(new SqlParameter("@ArtiklId", ID));
                    artiklSupermarketCommand.ExecuteNonQuery();
                }
            }
        }
        public void obrisiArtikl()
        {
            string deleteSql =
                "DELETE FROM T_SUPERMARKET_ARTIKL WHERE ArtiklId = @ArtiklId;" +
                "DELETE FROM T_ARTIKL WHERE ArtiklId = @ArtiklId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = deleteSql;
                command.Parameters.Add(new SqlParameter("@ArtiklId", ID));
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public List<Artikl> ucitajArtikle()
        {
            List<Artikl> artikli = new List<Artikl>();
            string queryString =
                "SELECT " +
                " super.SupermarketId, super.Ime, super.Mesto, super.Adresa, super.Telefon, " +
                " art.ArtiklId, art.Naziv, art.Cena, " +
                " dist.DistributerId, dist.ImeD, dist.Drzava, " +
                " vrst.VrstaId, vrst.NazivV " +
                "FROM T_SUPERMARKET super, T_ARTIKL art, T_SUPERMARKET_ARTIKL pa, T_DISTRIBUTER dist, T_VRSTA vrst " +
                "WHERE super.SupermarketId = super.SupermarketId AND art.ArtiklId = pa.ArtiklId AND " + " art.DistributerId = dist.DistributerId AND art.VrstaId = vrst.VrstaId " +
                "ORDER BY art.ArtiklId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    int prethodniIdArtikla = 0;
                    Artikl artikl = new Artikl();
                    while (reader.Read())
                    {
                        int idArtikla = Int32.Parse(reader["ArtiklId"].ToString());

                        if (idArtikla != prethodniIdArtikla)
                        {
                            prethodniIdArtikla = idArtikla;
                            artikl = new Artikl();
                            artikli.Add(artikl);
                            artikl.ID = idArtikla;

                            artikl.Naziv = reader["Naziv"].ToString();
                            artikl.Cena = reader["Cena"].ToString();

                            Distributer distributer = new Distributer();
                            distributer.ID = Int32.Parse(reader["DistributerId"].ToString());
                            distributer.ImeD = reader["ImeD"].ToString();
                            distributer.Drzava = reader["Drzava"].ToString();
                            artikl.Distributer = distributer;

                            Vrsta vrsta = new Vrsta();
                            vrsta.ID = Int32.Parse(reader["VrstaId"].ToString());
                            vrsta.NazivV = reader["NazivV"].ToString();
                            artikl.Vrsta = vrsta;

                            artikl.Supermarketi = new List<Supermarket>();
                        }
                        Supermarket supermarket = new Supermarket();

                        supermarket.ID = Int32.Parse(reader["SupermarketId"].ToString());
                        supermarket.Ime = reader["Ime"].ToString();
                        supermarket.Mesto = reader["Mesto"].ToString();
                        supermarket.Adresa = reader["Adresa"].ToString();
                        artikl.Supermarketi.Add(supermarket);
                    }
                }
            }
            return artikli;
        }

        public List<Artikl> ucitajArtikleZaVrste(int vrstaId)
        {
            List<Artikl> artikli = new List<Artikl>();
            string queryString =
                "SELECT " +
                " art.ArtiklId, art.Naziv, art.Cena, " +
                " vrst.VrstaId, vrst.NazivV " +
                "FROM T_ARTIKL art, T_VRSTA vrst WHERE art.VrstaId = vrst.VrstaId AND vrst.VrstaId = @VrstaId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                command.Parameters.Add(new SqlParameter("@VrstaId", vrstaId));
                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    Artikl art;
                    while (reader.Read())
                    {
                        art = new Artikl();
                        art.ID = Int32.Parse(reader["ArtiklId"].ToString());
                        art.Naziv = reader["Naziv"].ToString();
                        art.Cena = reader["Cena"].ToString();

                        art.Vrsta = new Vrsta();
                        art.Vrsta.ID = Int32.Parse(reader["VrstaId"].ToString());
                        art.Vrsta.NazivV = reader["NazivV"].ToString();

                        artikli.Add(art);
                    }
                }
            }
            return artikli;
        }
    }
}
