//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Supermarketi
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_SUPERMARKET
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_SUPERMARKET()
        {
            this.T_ARTIKL = new HashSet<T_ARTIKL>();
        }
    
        public int SupermarketId { get; set; }
        public string Ime { get; set; }
        public string Mesto { get; set; }
        public string Adresa { get; set; }
        public string Telefon { get; set; }
        public int MenadzerId { get; set; }
    
        public virtual T_MENADZER T_MENADZER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_ARTIKL> T_ARTIKL { get; set; }
    }
}
