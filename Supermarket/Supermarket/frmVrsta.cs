﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supermarketi
{
    public partial class frmVrsta : Form
    {
        List<Vrsta> vrsteList = new List<Vrsta>();
        string akcija = "";
        int indeksSelektovanog = -1;

        public frmVrsta()
        {

            InitializeComponent();

            dgVrsta.AllowUserToAddRows = false;
            dgVrsta.AllowUserToDeleteRows = false;
            dgVrsta.ReadOnly = true;
            dgVrsta.AutoGenerateColumns = false;

            dgVrsta.Columns.Add("ID", "ID");
            dgVrsta.Columns["ID"].Visible = false;
            dgVrsta.Columns.Add("nazivVrste", "Naziv");

            txtDisabled();
            btnChangeEnabled();
            btnSubmitDisabled();

            prikaziVrstuDGV();


        }
        private void txtDisabled()
        {
            txtNaziv.Enabled = false;
        }

        private void txtEnabled()
        {
            txtNaziv.Enabled = true;
        }

        private void btnChangeDisabled()
        {
            btnDodaj.Enabled = false;
            btnPromeni.Enabled = false;
            btnObrisi.Enabled = false;
        }

        private void btnChangeEnabled()
        {
            btnDodaj.Enabled = true;
            btnPromeni.Enabled = true;
            btnObrisi.Enabled = true;
        }

        private void btnSubmitDisabled()
        {
            btnPotvrdi.Enabled = false;
            btnOdustani.Enabled = false;
        }

        private void btnSubmitEnabled()
        {
            btnPotvrdi.Enabled = true;
            btnOdustani.Enabled = true;
        }

        private void ponistiUnosTxt()
        {
            txtNaziv.Text = "";
        }

        private void prikaziVrstuTxt()
        {
            int idSelektovanog = (int)dgVrsta.SelectedRows[0].Cells["ID"].Value;
            Vrsta selektovanaVrsta =
                vrsteList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
            if (selektovanaVrsta != null)
            {
                txtNaziv.Text = selektovanaVrsta.NazivV;
            }
        }

        private void prikaziVrstuDGV()
        {
            vrsteList = new Vrsta().ucitajVrste();
            dgVrsta.Rows.Clear();
            for (int i = 0; i < vrsteList.Count; i++)
            {
                dgVrsta.Rows.Add();
                dgVrsta.Rows[i].Cells["ID"].Value =
                    vrsteList[i].ID;
                dgVrsta.Rows[i].Cells["nazivVrste"].Value =
                    vrsteList[i].NazivV;

            }
            ponistiUnosTxt();
            dgVrsta.CurrentCell = null;
            if (vrsteList.Count > 0)
            {
                if (indeksSelektovanog != -1)
                    dgVrsta.Rows[indeksSelektovanog].Selected = true;
                else
                    dgVrsta.Rows[0].Selected = true;
                prikaziVrstuTxt();
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            ponistiUnosTxt();
            txtEnabled();
            btnSubmitEnabled();
            btnChangeDisabled();
            akcija = "dodaj";
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgVrsta.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Da li zelite da obrisete odabranu vrstu?",
                "Potvrda brisanja", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int idSelektovanog = (int)dgVrsta.SelectedRows[0].Cells["ID"].Value;
                    Vrsta selektovaniRok = vrsteList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
                    if (selektovaniRok != null)
                    {
                        selektovaniRok.obrisiVrstu();
                    }
                    indeksSelektovanog = -1;
                    prikaziVrstuDGV();
                }
            }
            else
            {
                MessageBox.Show("Nema unetih podataka");
            }
        }

        private void btnPromeni_Click(object sender, EventArgs e)
        {
            if (dgVrsta.SelectedRows.Count > 0)
            {
                txtEnabled();
                btnSubmitEnabled();
                btnChangeDisabled();
                akcija = "promeni";
            }
            else
            {
                MessageBox.Show("Nema unetih podataka");
            }
        }

        private void btnPotvrdi_Click(object sender, EventArgs e)
        {
            try
            {
                if (akcija == "promeni")
                {
                    int idSelektovanog = (int)dgVrsta.SelectedRows[0].Cells["ID"].Value;
                    Vrsta selektovanaVrsta = vrsteList.Where(x => x.ID == idSelektovanog).FirstOrDefault();

                    selektovanaVrsta.NazivV = txtNaziv.Text;
                    selektovanaVrsta.azurirajVrstu();
                    indeksSelektovanog = dgVrsta.SelectedRows[0].Index;
                }
                else if (akcija == "dodaj")
                {
                    Vrsta vrsta = new Vrsta();
                    vrsta.NazivV = txtNaziv.Text;
                    vrsta.dodajVrstu();
                    indeksSelektovanog = dgVrsta.Rows.Count;
                }
                txtDisabled();
                btnSubmitDisabled();
                btnChangeEnabled();
                akcija = "";
                prikaziVrstuDGV();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            txtDisabled();
            btnSubmitDisabled();
            btnChangeEnabled();
        }

        private void dgVrsta_CellClick(object sender,
            DataGridViewCellEventArgs e)
        {
            if (dgVrsta.CurrentRow != null)
            {
                dgVrsta.Rows[dgVrsta.CurrentRow.Index].Selected = true;
                prikaziVrstuTxt();
            }
        }

    }
}
