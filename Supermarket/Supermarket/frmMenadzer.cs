﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supermarketi
{
    public partial class frmMenadzer : Form
    {

        List<Menadzer> menadzeriList = new List<Menadzer>();
        string akcija = "";
        int indeksSelektovanog = -1;
        public frmMenadzer()
        {
            InitializeComponent();
            dgMenadzeri.AllowUserToAddRows = false;
            dgMenadzeri.AllowUserToDeleteRows = false;
            dgMenadzeri.ReadOnly = true;
            dgMenadzeri.AutoGenerateColumns = false;

            dgMenadzeri.Columns.Add("ID", "ID");
            dgMenadzeri.Columns["ID"].Visible = false;
            dgMenadzeri.Columns.Add("imeMenadzera", "ImeM");
            dgMenadzeri.Columns.Add("prezimeMenadzera", "Prezime");
            dgMenadzeri.Columns.Add("telefonMenadzera", "Telefon");

            txtDisabled();
            btnChangeEnabled();
            btnSubmitDisabled();

            prikaziMenadzereDGV();
        }
        private void txtDisabled()
        {
            txtIme.Enabled = false;
            txtPrezime.Enabled = false;
            txtTelefon.Enabled = false;
        }

        private void txtEnabled()
        {
            txtIme.Enabled = true;
            txtPrezime.Enabled = true;
            txtTelefon.Enabled = true;
        }

        private void btnChangeDisabled()
        {
            btnDodaj.Enabled = false;
            btnPromeni.Enabled = false;
            btnObrisi.Enabled = false;
        }

        private void btnChangeEnabled()
        {
            btnDodaj.Enabled = true;
            btnPromeni.Enabled = true;
            btnObrisi.Enabled = true;
        }

        private void btnSubmitDisabled()
        {
            btnPotvrdi.Enabled = false;
            btnOdustani.Enabled = false;
        }

        private void btnSubmitEnabled()
        {
            btnPotvrdi.Enabled = true;
            btnOdustani.Enabled = true;
        }

        private void ponistiUnosTxt()
        {
            txtIme.Text = "";
            txtPrezime.Text = "";
            txtTelefon.Text = "";
        }

        private void prikaziMenadzeraTxt()
        {
            int idSelektovanog = (int)dgMenadzeri.SelectedRows[0].Cells["ID"].Value;
            Menadzer selektovaniMenadzer =
                menadzeriList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
            if (selektovaniMenadzer != null)
            {
                txtIme.Text = selektovaniMenadzer.ImeM;
                txtPrezime.Text = selektovaniMenadzer.Prezime;
                txtTelefon.Text = selektovaniMenadzer.TelefonM;
            }
        }

        private void prikaziMenadzereDGV()
        {
            menadzeriList = new Menadzer().ucitajMenadzere();
            dgMenadzeri.Rows.Clear();
            for (int i = 0; i < menadzeriList.Count; i++)
            {
                dgMenadzeri.Rows.Add();
                dgMenadzeri.Rows[i].Cells["ID"].Value =
                    menadzeriList[i].ID;
                dgMenadzeri.Rows[i].Cells["imeMenadzera"].Value =
                    menadzeriList[i].ImeM;
                dgMenadzeri.Rows[i].Cells["prezimeMenadzera"].Value =
                    menadzeriList[i].Prezime;
                dgMenadzeri.Rows[i].Cells["telefonMenadzera"].Value =
                    menadzeriList[i].TelefonM;

            }
            ponistiUnosTxt();
            dgMenadzeri.CurrentCell = null;
            if (menadzeriList.Count > 0)
            {
                if (indeksSelektovanog != -1)
                    dgMenadzeri.Rows[indeksSelektovanog].Selected = true;
                else
                    dgMenadzeri.Rows[0].Selected = true;
                prikaziMenadzeraTxt();
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            ponistiUnosTxt();
            txtEnabled();
            btnSubmitEnabled();
            btnChangeDisabled();
            akcija = "dodaj";
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgMenadzeri.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Da li zelite da obrisete odabranog menadzera?",
                "Potvrda brisanja", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int idSelektovanog = (int)dgMenadzeri.SelectedRows[0].Cells["ID"].Value;
                    Menadzer selektovaniMenadzer = menadzeriList.Where(x => x.ID == idSelektovanog).FirstOrDefault();
                    if (selektovaniMenadzer != null)
                    {
                        selektovaniMenadzer.obrisiMenadzera();
                    }
                    indeksSelektovanog = -1;
                    prikaziMenadzereDGV();
                }
            }
            else
            {
                MessageBox.Show("Nema unetih podataka");
            }
        }

        private void btnPromeni_Click(object sender, EventArgs e)
        {
            if (dgMenadzeri.SelectedRows.Count > 0)
            {
                txtEnabled();
                btnSubmitEnabled();
                btnChangeDisabled();
                akcija = "promeni";
            }
            else
            {
                MessageBox.Show("Nema unetih podataka");
            }
        }

        private void btnPotvrdi_Click(object sender, EventArgs e)
        {
            try
            {
                if (akcija == "promeni")
                {
                    int idSelektovanog = (int)dgMenadzeri.SelectedRows[0].Cells["ID"].Value;
                    Menadzer selektovaniMenadzer = menadzeriList.Where(x => x.ID == idSelektovanog).FirstOrDefault();

                    selektovaniMenadzer.ImeM = txtIme.Text;
                    selektovaniMenadzer.Prezime = txtPrezime.Text;
                    selektovaniMenadzer.TelefonM = txtTelefon.Text;
                    selektovaniMenadzer.azurirajMenadzera();
                    indeksSelektovanog = dgMenadzeri.SelectedRows[0].Index;
                }
                else if (akcija == "dodaj")
                {
                    Menadzer men = new Menadzer();
                    men.ImeM = txtIme.Text;
                    men.Prezime = txtPrezime.Text;
                    men.TelefonM = txtTelefon.Text;
                    men.dodajMenadzera();
                    indeksSelektovanog = dgMenadzeri.Rows.Count;
                }
                txtDisabled();
                btnSubmitDisabled();
                btnChangeEnabled();
                akcija = "";
                prikaziMenadzereDGV();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnOdustani_Click(object sender, EventArgs e)
        {
            txtDisabled();
            btnSubmitDisabled();
            btnChangeEnabled();
        }

        private void dgMenadzeri_CellClick(object sender,
            DataGridViewCellEventArgs e)
        {
            if (dgMenadzeri.CurrentRow != null)
            {
                dgMenadzeri.Rows[dgMenadzeri.CurrentRow.Index].Selected = true;
                prikaziMenadzeraTxt();
            }

        }
    }
}
