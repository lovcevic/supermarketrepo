﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supermarketi
{
    [Serializable]
    class Distributer
    {
        private int id;
        private string imeD;
        private string drzava;
        private string mesto;
        private string adresa;
        private int ptt;
        private string telefon;


        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string ImeD
        {
            get { return imeD; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti ime distributera!!!");
                imeD = value;
            }
        }

        public string Drzava
        {
            get { return drzava; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti drzavu distributera!!!");
                drzava = value;
            }
        }

        public string Mesto
        {
            get { return mesto; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti mesto distributera!!!");
                mesto = value;
            }
        }

        public string Adresa
        {
            get { return adresa; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti adresu distributera!!!");
                adresa = value;
            }
        }

        public int Ptt
        {
            get { return ptt; }
            set
            {
                if (value == 0)
                    throw new Exception("Morate uneti PTT!!!");
                ptt = value;
            }
        }

        public string Telefon
        {
            get { return telefon; }
            set
            {
                if (value == "")
                    throw new Exception("Morate uneti broj telefona!!!");
                telefon = value;
            }
        }


        private string _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\SupermarketiDB.mdf;Integrated Security=True";

        public void dodajDistributera()
        {
            string insertSql = "INSERT INTO T_DISTRIBUTER " +
                "(ImeD, Drzava, Mesto, Adresa, Ptt, Telefon) VALUES " +
                "(@ImeD, @Drzava, @Mesto, @Adresa, @Ptt, @Telefon)";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = insertSql;
                command.Parameters.Add(new SqlParameter("@ImeD", ImeD));
                command.Parameters.Add(new SqlParameter("@Drzava", Drzava));
                command.Parameters.Add(new SqlParameter("@Mesto", Mesto));
                command.Parameters.Add(new SqlParameter("@Adresa", Adresa));
                command.Parameters.Add(new SqlParameter("@Ptt", Ptt));
                command.Parameters.Add(new SqlParameter("@Telefon", Telefon));
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void azurirajDistributera()
        {
            string updateSql =
                "UPDATE T_DISTRIBUTER " +
                "SET ImeD = @ImeD, Drzava = @Drzava, Mesto = @Mesto, Adresa = @Adresa, Ptt = @Ptt, Telefon = @Telefon " +
                "WHERE DistributerId = @DistributerId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = updateSql;
                command.Parameters.Add(new SqlParameter("@DistributerId", ID));
                command.Parameters.Add(new SqlParameter("@ImeD", ImeD));
                command.Parameters.Add(new SqlParameter("@Drzava", Drzava));
                command.Parameters.Add(new SqlParameter("@Mesto", Mesto));
                command.Parameters.Add(new SqlParameter("@Adresa", Adresa));
                command.Parameters.Add(new SqlParameter("@Ptt", Ptt));
                command.Parameters.Add(new SqlParameter("@Telefon", Telefon));
                connection.Open();
                command.ExecuteNonQuery();
            }

        }

        public void obrisiDistributera()
        {
            StringBuilder errorMessages = new StringBuilder();
            string deleteSql =
                "DELETE FROM T_DISTRIBUTER WHERE DistributerId = @DistributerId;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = deleteSql;
                command.Parameters.Add(new SqlParameter("@DistributerId", ID));
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (SqlException)
                {
                    MessageBox.Show("podatak je vezan za Artikl");
                }
            }
        }

        public List<Distributer> ucitajDistributere()
        {
            List<Distributer> distributeri = new List<Distributer>();
            string queryString =
            "SELECT * FROM T_DISTRIBUTER;";
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = queryString;
                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    Distributer dist;
                    while (reader.Read())
                    {
                        dist = new Distributer();
                        dist.ID = Int32.Parse(reader["DistributerId"].ToString());
                        dist.ImeD = reader["ImeD"].ToString();
                        dist.Drzava = reader["Drzava"].ToString();
                        dist.Mesto = reader["Mesto"].ToString();
                        dist.Adresa = reader["Adresa"].ToString();
                        dist.Ptt = Int32.Parse(reader["Ptt"].ToString());
                        dist.Telefon = reader["Telefon"].ToString();
                        distributeri.Add(dist);
                    }
                }
            }

            return distributeri;
        }

    }
}

